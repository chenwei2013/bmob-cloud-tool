onRequest = (request, response, modules) ->
  longitude = parseFloat request.body.lon # double
  latitude = parseFloat request.body.lat # double
  distance = parseFloat request.body.dist # double in meter
  limit = request.body.limit.split(",")
  uid = request.body.uid
  timestamp = parseFloat request.body.timestamp # long

  offset = parseInt limit[0]
  pagesize = parseInt limit[1]

  db = modules.oData
  succCode = "SUCCESS"
  failCode = "FAILURE"

  procErr = (err) ->
    errObj = JSON.parse(err)
    ret = 
      code: failCode + "," + errObj.code
      message: errObj.message
    response.end JSON.stringify(ret)
  procRet = (code, content, message) ->
    ret =
      code: code
      content: content
      message: message
    response.end JSON.stringify(ret)

  queryNearbyOp =
    "table": "Group"
    "limit": pagesize
    "skip" : offset
    "where": {
      "status": 1
      "location": { 
        "$nearSphere": {
          "__type": "GeoPoint" 
          "longitude": longitude
          "latitude": latitude
        }
        "$maxDistanceInKilometers": 7000 #distance/1000
      }
      "createTimestamp": { "$lte": timestamp }
    }
  queryNearby = () -> 
    db.find queryNearbyOp, (err, data) -> 
      if err then procErr err
      else
        # return success code 
        nearbyGroupObj = JSON.parse(data).results
        queryJoinedGroups nearbyGroupObj
        
  queryJoinedGroupsOp =
    "table": "DeviceGroup"
    "where": { "uid": uid }
  queryJoinedGroups = (nearbyGroupObj) ->
    db.find queryJoinedGroupsOp, (err, data) ->
      joinedGroups = JSON.parse(data).results.map (g) -> g.groupid
      items = []
      for nearbyGroup in nearbyGroupObj
        groupid = nearbyGroup.groupid
        joined = if groupid in joinedGroups then 1 else 0
        item = 
          groupid: groupid
          longitude: nearbyGroup.location.longitude
          latitude: nearbyGroup.location.latitude
          name: nearbyGroup.name
          joined: joined
        items.push item
      content =
        count: nearbyGroupObj.length
        items: items
      procRet succCode, content, "Fetch nearby chatgroups successful."

  queryNearby()

exports.v1_chatgroupNearby = onRequest;                                                                   