onRequest = (request, response, modules) ->
  longitude = request.body.longitude
  latitude = request.body.latitude
  groupid = request.body.groupid
  name = request.body.name
  effectDistance = request.body.effectDistance
  avatar = request.body.avatar
  # ownerDevice = request.body.ownerDevice
  uid = request.body.uid

  db = modules.oData
  succCode = "SUCCESS"
  failCode = "FAILURE"
  currentTimestamp = new Date().getTime()

  procErr = (err) ->
    errObj = JSON.parse(err)
    ret = 
      code: failCode + "," + errObj.code
      message: errObj.message
    response.end JSON.stringify(ret)
  procRet = (code, message) ->
    ret =
      code: code
      message: message
    response.end JSON.stringify(ret)

  insertGroupOp =
    "table": "Group"
    "data" : {
      "groupid": groupid
      "name": name
      "status": 1
      "effectDistance": parseInt effectDistance
      "location": { 
        "__type": "GeoPoint" 
        "longitude": parseFloat longitude
        "latitude": parseFloat latitude
      }
      "avatar": {
        "__type": "Pointer"
        "className": "GroupAvatar"
        "objectId": avatar
      }
      "ownerDevice": {
        "__type": "Pointer"
        "className": "Device"
        "objectId": ""
      }
      "joinedDevices": {
        "__op": "AddRelation"
        "objects": [{
          "__type": "Pointer"
          "className": "Device"
          "objectId": ""
        }]
      }
      "createTimestamp": currentTimestamp
      "updateTimestamp": currentTimestamp
    }
  insertGroup = (op) -> 
    db.insert op, (err, data) -> 
      if err then procErr err
      else
        # add a record to DeviceGroup table 
        queryDeviceGroup()

  insertDeviceGroupOp =
    "table": "DeviceGroup"
    "data" : { "uid": uid, "groupid": groupid }
  insertDeviceGroup = () ->
    db.insert insertDeviceGroupOp, (err, data) ->
      if err then procErr err
      # return success code 
      else procRet succCode, "New chatgroup successful."
  queryDeviceGroupOp =
    "table": "DeviceGroup"
    "limit": 1
    "count": 1
    "where": { "uid": uid, "groupid": groupid }
  queryDeviceGroup = () ->
    db.find queryDeviceGroupOp, (err, data) -> # query device by uid
      if err then procErr err
      else # insert the new chatgroup to table
        dataObj = JSON.parse(data)
        count = dataObj.count
        if count > 0
          procRet succCode, "Device already joined the group."
        else insertDeviceGroup()

  queryDeviceOp =
    "table": "Device"
    "limit": 1
    "count": 1
    "where": { "uid": uid }
  queryDevice = () ->
    db.find queryDeviceOp, (err, data) -> # query device by uid
      if err then procErr err
      else # insert the new chatgroup to table
        dataObj = JSON.parse(data)
        count = dataObj.count
        if count > 0
          objectId = dataObj.results[0].objectId
          idata = insertGroupOp.data
          idata.ownerDevice.objectId = objectId
          idata.joinedDevices.objects[0].objectId = objectId
          insertGroup insertGroupOp
        else procRet failCode, "The device with '" + uid + "' not registered."

  query =
    "table": "Group"
    "limit": 0
    "count": 1
    "where": { "groupid": groupid }
  db.find query, (err, data) -> # query group by groupid
    if err then procErr err
    else 
      dataObj = JSON.parse(data)
      count = dataObj.count
      if count > 0
        procRet succCode, "Chatgroup already exists."
      else # query device and insert the group
        console.log "Executing queryDevice..."
        queryDevice()

exports.v2_chatgroupNew = onRequest;                                                                   