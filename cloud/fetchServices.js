// Generated by CoffeeScript 1.7.1
var onRequest;

onRequest = function(request, response, modules) {
  var category, db, findop;
  category = request.body.category;
  db = modules.oData;
  findop = {
    "table": "ServiceProvider",
    "where": {
      "category": parseInt(category),
      "valid": 1
    }
  };
  return db.find(findop, function(err, data) {
    var address, item, itemarr, name, phone, provider, resparr, _i, _len;
    if (err) {
      return response.end("error is " + err.code + " error message is " + err.error);
    } else {
      itemarr = JSON.parse(data).results;
      resparr = [];
      for (_i = 0, _len = itemarr.length; _i < _len; _i++) {
        item = itemarr[_i];
        name = item.name;
        address = item.address;
        phone = item.phone;
        provider = {
          "name": name,
          "address": address,
          "phone": phone
        };
        resparr.push(provider);
      }
      return response.end(JSON.stringify(resparr));
    }
  });
};

exports.fetchServices = onRequest;
