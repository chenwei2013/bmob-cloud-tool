onRequest = (request, response, modules) ->
  groupid = request.body.groupid
  uid = request.body.uid

  db = modules.oData
  succCode = "SUCCESS"
  failCode = "FAILURE"

  procErr = (err) ->
    errObj = JSON.parse(err)
    ret = 
      code: failCode + "," + errObj.code
      message: errObj.message
    response.end JSON.stringify(ret)
  procRet = (code, message) ->
    ret =
      code: code
      message: message
    response.end JSON.stringify(ret)

  queryDeviceGroupOp =
    "table": "DeviceGroup"
    "limit": 0
    "count": 1
    "where" : { "uid": uid, "groupid": groupid }
  joinDeviceGroup = () ->
    message = "Uid already joined groupid."
    db.find queryDeviceGroupOp, (err, data) ->
      if err then procErr err
      else
        dataObj = JSON.parse(data)
        count = dataObj.count
        if count > 0
          procRet succCode, message
        else insertDeviceGroup()

  insertDeviceGroupOp =
    "table": "DeviceGroup"
    "data" : { "uid": uid, "groupid": groupid }
  insertDeviceGroup = () ->
    db.insert insertDeviceGroupOp, (err, data) ->
      if err then procErr err
      else procRet succCode, "Join a group successful."

  joinDeviceGroup()

exports.v1_chatgroupJoin = onRequest;                                                                   