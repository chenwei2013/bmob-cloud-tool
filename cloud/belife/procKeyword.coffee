onRequest = (request, response, modules) ->
    keyword = request.body.keyword
    db = modules.oData

    findop = 
        "table":"ServiceCategory"
        "where":{"title":keyword}

    db.find findop, (err, data) -> 
        if err 
            response.end "error is " + err.code  + " error message is " + err.error
        else 
            itemarr = JSON.parse(data).results
            
            if itemarr and itemarr.length > 0
                item = itemarr[0]
                category = item.category
                # Call fetchServices cloud code
                functions = modules.oFunctions

                functions.run {
                    "name": "fetchServices"
                    "data": {"category": category}
                }, (err, data) ->
                    if err
                        response.end "error is " + err.code  + " error message is " + err.error
                    else
                        response.end data
            else # keyword is category
                findopKeyCategory = 
                    "table":"ServiceCategory"
                    "where":{"category":keyword}
                db.find findopKeyCategory, (err, data) ->
                    if err
                        response.end "error is " + err.code  + " error message is " + err.error
                    else
                        itemarrCategory = JSON.parse(data).results
                        if itemarrCategory and itemarrCategory.length > 0
                            category = keyword
                        response.end ""

exports.procKeyword = onRequest;                                                                   