onRequest = (request, response, modules) ->
  objectId = request.body.objectId
  
  db = modules.oData

  findop = 
    "table": "ServiceProvider"
    "objectId": objectId

  db.findOne findop, (err, data) -> 
    if err 
      response.end "error is " + err.code  + " error message is " + err.error
    else 
      detail = JSON.parse(data)
      provider = 
        "name": detail.name,
        "address": detail.address,
        "phones": detail.phones
      
      response.end JSON.stringify(provider)

exports.v1_detail = onRequest;