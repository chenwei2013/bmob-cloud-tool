onRequest = (request, response, modules) ->
  deviceUuid = request.body.deviceUuid

  db = modules.oData
  query =
    "table": "Device"
    "limit": 1
    "count": 1
    "where": { "deviceUuid": deviceUuid }
  op =
    "table": "Device"
    "data" : {
        "deviceUuid": deviceUuid
    }

  db.find query, (err, data) ->
    succCode = "SUCCESS"
    failCode = "FAILURE"
    if err 
      ret = 
        code: failCode
        message: err.error
      response.end JSON.stringify(ret)
    else 
      dataObj = JSON.parse(data)
      count = dataObj.count
      if count > 0
        ret = 
          code: succCode
          content: { id: dataObj.results[0].uid }
          message: "Device already registered."
        response.end JSON.stringify(ret)
      else 
        db.insert op, (err, data) -> 
          if err 
            ret =
              code: failCode
              message: err.error
            response.end JSON.stringify(ret)
          else 
            dataObj = JSON.parse(data)
            updateop = 
              "table": "Device"
              "objectId": dataObj.objectId
              "data": { "uid": dataObj.objectId }
            db.update updateop, (err, data) -> 
              if err then console.log err
            ret =
              code: succCode
              content: { id: dataObj.objectId }
              message: "New device successful."
            response.end JSON.stringify(ret)

exports.v1_deviceInit = onRequest;                                                                   