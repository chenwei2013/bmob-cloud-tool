onRequest = (request, response, modules) ->
  groupid = request.body.groupid
  uid = request.body.uid

  db = modules.oData
  succCode = "SUCCESS"
  failCode = "FAILURE"

  procErr = (err) ->
    errObj = JSON.parse(err)
    ret = 
      code: failCode + "," + errObj.code
      message: errObj.message
    response.end JSON.stringify(ret)
  procRet = (code, message) ->
    ret =
      code: code
      message: message
    response.end JSON.stringify(ret)

  leaveDeviceGroupOp =
    "table": "DeviceGroup"
    "limit": 1
    "count": 1
    "where" : { "uid": uid, "groupid": groupid }
  leaveDeviceGroup = () ->
    message = "The corresponding record is not found."
    db.find leaveDeviceGroupOp, (err, data) ->
      if err then procErr err
      else
        dataObj = JSON.parse(data)
        count = dataObj.count
        if count > 0
          objectId = dataObj.results[0].objectId
          removeRecordbyObjectId objectId
        else procRet failCode, message

  removeRecordbyObjectId = (objectId) ->
    removeOp = 
      "table": "DeviceGroup"
      "objectId": objectId
    db.remove removeOp, (err, data) ->
      if err then procErr err
      else procRet succCode, "Leave a group successful."

  leaveDeviceGroup()

exports.v1_chatgroupLeave = onRequest;                                                                   