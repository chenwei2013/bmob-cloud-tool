onRequest = (request, response, modules) ->
  category = parseInt request.body.category # int

  db = modules.oData
  succCode = "SUCCESS"
  failCode = "FAILURE"

  procErr = (err) ->
    errObj = JSON.parse(err)
    ret = 
      code: failCode + "," + errObj.code
      message: errObj.message
    response.end JSON.stringify(ret)
  procRet = (code, content, message) ->
    ret =
      code: code
      content: content
      message: message
    response.end JSON.stringify(ret)

  queryTitleOp =
    "table": "ServiceCategory"
    "limit": 1
    "where": { "category": category }
  queryTitle = -> 
    db.find queryTitleOp, (err, data) -> 
      if err then procErr err
      else
        # return success code 
        results = JSON.parse(data).results
        if results.length
          title = results[0].title
          queryServices title
        else
          procRet failCode, null, 'Cannot find category ' + category
        
  queryServicesOp =
    "table": "ServiceProvider"
    "limit": 3
    "where": { "category": category, "valid": 1 }
  queryServices = (title) ->
    db.find queryServicesOp, (err, data) ->
      services = JSON.parse(data).results.map (s) -> s.objectId
      content =
        title: title
        items: services
      procRet succCode, content, 'Fetch services successful.'

  queryTitle()

exports.v1_service = onRequest;                                                                   