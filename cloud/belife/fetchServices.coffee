onRequest = (request, response, modules) ->
    category = request.body.category
    db = modules.oData

    findop = 
        "table":"ServiceProvider"
        "where":{"category":parseInt(category),"valid":1}

    db.find findop, (err, data) -> 
        if err 
            response.end "error is " + err.code  + " error message is " + err.error
        else 
            itemarr = JSON.parse(data).results
            resparr = []
            
            for item in itemarr
                name = item.name
                address = item.address
                phone = item.phone
                provider = 
                    "name": name,
                    "address": address,
                    "phone": phone
                
                resparr.push provider
            
            response.end JSON.stringify(resparr)

exports.fetchServices = onRequest;                                                                     